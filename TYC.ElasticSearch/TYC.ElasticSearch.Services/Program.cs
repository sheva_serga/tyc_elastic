﻿using System;
using TYC.JsonExport.Common.Models;
using TYC.JsonExport.Common.Models.ElasticSearch;
using TYC.JsonExport.Services;
using TYC.JsonExport.Services.DbStore;
using TYC.JsonExport.Services.DbStore.Queries;
using TYC.JsonExport.Services.Interfaces;
using TYC.JsonExport.Services.Search;

namespace TYC.JsonExport
{
    class Program
    {
        private const String ES_CONNECTION = "http://localhost:9200";
        private const String SQL_CONNECTION = "Data Source=.;Initial Catalog=TYC20;Integrated Security=SSPI;MultipleActiveResultSets=true;";

        static void Main(string[] args)
        {
            IDbStoreService storeService = new DbStoreService(SQL_CONNECTION);

            //IDataExporter<Transaction> exporter1 = new SqlTransactionDataExporter(storeService);
            //IDataExporter<TransactionTask> exporter2 = new SqlTasksDataExporter(storeService);
            //
            //ISearchDataImporter dataImporter = new ElasticDataImporter(ES_CONNECTION, exporter1, exporter2);
            //dataImporter.IndexDocuemnts();

            Int32 userId = 2;
            IDbUserService userService = new DbUserService(storeService);
            IDbOrganizationNodeService orgNodeService = new DbOrganizationNodeService(storeService);

            IUser user = userService.GetUser(userId);
            

            ITaskSearchService taskSearch = new ElasticTaskSearchService(ES_CONNECTION, orgNodeService);
            var tasks = taskSearch.SearchTasks(user, 9);

            ITransactionSearchServise search = new ElasticTransactionSearchService(ES_CONNECTION, orgNodeService);
            var transactions = search.SearchTransactions(user);
        }
    }
}
