﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYC.JsonExport.Common.Models.ElasticSearch;
using TYC.JsonExport.Services.Interfaces;

namespace TYC.JsonExport.Services
{
    internal class ElasticDataImporter : ISearchDataImporter
    {
        private const String INDEX_NAME = "transactions_index_with_tasks";

        private readonly Uri _nodeUri;
        private readonly IDataExporter<TransactionTask> _taskExporter;
        private readonly IDataExporter<Transaction> _transactionExporter;

        public ElasticDataImporter(
              String uri
            , IDataExporter<Transaction> transactionExporter
            , IDataExporter<TransactionTask> taskExporter)
        {
            _nodeUri = new Uri(uri);
            _taskExporter = taskExporter;
            _transactionExporter = transactionExporter;
        }

        public void IndexDocuemnts()
        {
            IEnumerable<TransactionTask> tasks = null;
            IEnumerable<Transaction> transactions = null;

            ElasticClient client = new ElasticClient(_nodeUri);

            if (client.IndexExists(new IndexExistsRequest(INDEX_NAME)).Exists)
                client.DeleteIndex(new DeleteIndexRequest(INDEX_NAME));

            var indexConfig = new IndexState() {
                Settings = new IndexSettings { NumberOfReplicas = 1, NumberOfShards = 5 }
            };

            var result = client.CreateIndex(INDEX_NAME, s => s
                .InitializeUsing(indexConfig)
                .Mappings(m => m
                    .Map<RelationDocumentBase>(t => t
                        .RoutingField(r => r.Required())
                        .AutoMap<Transaction>()
                        .AutoMap<TransactionTask>()
                        .Properties(props => props
                            .Join(j => j
                                .Name(p => p.Join)
                                .Relations(r => r
                                    .Join<Transaction, TransactionTask>()
                                )
                            )
                        )
                    )
                )
            );

            if (result.ApiCall.Success)
                Task.WaitAll(
                      Task.Run(async () => tasks = await _taskExporter.GetDataAsync())
                    , Task.Run(async () => transactions = await _transactionExporter.GetDataAsync())
                );
            else throw new Exception("Indext was not created"); ;

            var transIndexResult = client.IndexMany<RelationDocumentBase>(transactions, INDEX_NAME);
            if (!transIndexResult.ApiCall.Success) throw new Exception("Transaction Indexing Error");

            var taskIndexResult = client.IndexMany<RelationDocumentBase>(tasks, INDEX_NAME);
            if (!taskIndexResult.ApiCall.Success) throw new Exception("Tasks Indexing Error");
        }
    }
}
