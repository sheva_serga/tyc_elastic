﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Services.Interfaces
{
    public interface IDataExporter<T> where T : class
    {
        IEnumerable<T> GetData();
        Task<IEnumerable<T>> GetDataAsync();
    }
}
