﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Services.DbStore
{
    public interface IDbStoreService
    {
        DbConnection CreateConnection();

        T Select<T>(String query, Object parameters = null);
        Int32 Execute(String query, Object parameters = null);

        List<T> SelectList<T>(String query, Object parameters = null);
        List<TResult> SelectList<T, TResult>(String query, Object parameters = null) where T : TResult;

        Task<T> SelectAsync<T>(String query, Object parameters = null);
        Task<Int32> ExecuteAsync(String query, Object parameters = null);

        Task<List<T>> SelectListAsync<T>(String query, Object parameters = null);
        Task<List<TResult>> SelectListAsync<T, TResult>(String query, Object parameters = null) where T : TResult;
    }
}
