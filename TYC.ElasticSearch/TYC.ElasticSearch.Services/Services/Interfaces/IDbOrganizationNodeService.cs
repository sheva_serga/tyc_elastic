﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Services.Interfaces
{
    public interface IDbOrganizationNodeService
    {
        List<Int32> GetTaskSearchHierarchy(Int32 userId);
        List<Int32> GetUserExtendedHierarchy(Int32 userId);
    }
}
