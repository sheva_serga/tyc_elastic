﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYC.JsonExport.Common.Models;

namespace TYC.JsonExport.Services.Interfaces
{
    public interface IDbUserService
    {
        IUser GetUser(Int32 userId);
    }
}
