﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYC.JsonExport.Common.Models;
using TYC.JsonExport.Common.Models.ElasticSearch;

namespace TYC.JsonExport.Services.Interfaces
{
    public interface ITaskSearchService
    {
        ISearchResponse<TransactionTask> SearchTasks(IUser user, Int32 groupId);
    }
}
