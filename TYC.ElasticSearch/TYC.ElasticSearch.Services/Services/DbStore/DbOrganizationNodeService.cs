﻿using System;
using System.Collections.Generic;
using TYC.JsonExport.Services.DbStore.Queries;
using TYC.JsonExport.Services.Interfaces;

namespace TYC.JsonExport.Services.DbStore
{
    internal class DbOrganizationNodeService : IDbOrganizationNodeService
    {
        private readonly IDbStoreService _storeService;

        public DbOrganizationNodeService(IDbStoreService storeService)
        {
            _storeService = storeService;
        }

        public List<Int32> GetUserExtendedHierarchy(Int32 userId) =>
            _storeService.SelectList<Int32>(OrganizationNodeQueries.GET_HIERARCHY, new { userId });

        public List<Int32> GetTaskSearchHierarchy(Int32 userId) =>
            _storeService.SelectList<Int32>(OrganizationNodeQueries.TASK_SEARCH_GET_HIERARCHY, new { userId });

    }
}
