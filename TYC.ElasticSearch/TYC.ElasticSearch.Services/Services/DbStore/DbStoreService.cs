﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Services.DbStore.Queries
{
    internal class DbStoreService : IDbStoreService
    {
        private readonly String _connectioString;

        public DbStoreService(String connectionString)
        {
            _connectioString = connectionString;
        }

        private SqlConnection CreateConnection() => new SqlConnection(_connectioString);

        private async Task<Int32> ExecuteAsync(String query, Object parameters)
        {
            using (IDbConnection connection = CreateConnection())
            {
                return await connection.ExecuteAsync(query, parameters);
            }
        }

        private async Task<T> SelectAsync<T>(String query, Object parameters)
        {
            using (IDbConnection connection = CreateConnection())
            {
                return (await connection.QueryAsync<T>(query, parameters)).FirstOrDefault();
            }
        }

        private async Task<List<TResult>> SelectListAsync<T, TResult>(String query, Object parameters) where T : TResult
        {
            using (IDbConnection connection = CreateConnection())
            {
                return (await connection.QueryAsync<T>(query, parameters)).Cast<TResult>().ToList();
            }
        }

        private Int32 Execute(String query, Object parameters)
        {
            using (IDbConnection connection = CreateConnection())
            {
                return connection.Execute(query, parameters);
            }
        }

        private T Select<T>(String query, Object parameters)
        {
            using (IDbConnection connection = CreateConnection())
            {
                return connection.Query<T>(query, parameters).FirstOrDefault();
            }
        }

        private List<TResult> SelectList<T, TResult>(String query, Object parameters) where T : TResult
        {
            using (IDbConnection connection = CreateConnection())
            {
                return connection.Query<T>(query, parameters).Cast<TResult>().ToList();
            }
        }

        DbConnection IDbStoreService.CreateConnection() => CreateConnection();

        T IDbStoreService.Select<T>(String query, Object parameters) => Select<T>(query, parameters);
        Int32 IDbStoreService.Execute(String query, Object parameters) => Execute(query, parameters);

        List<T> IDbStoreService.SelectList<T>(String query, Object parameters) => SelectList<T, T>(query, parameters);
        List<TResult> IDbStoreService.SelectList<T, TResult>(String query, Object parameters) => SelectList<T, TResult>(query, parameters);

        async Task<T> IDbStoreService.SelectAsync<T>(String query, Object parameters) => await SelectAsync<T>(query, parameters);
        async Task<Int32> IDbStoreService.ExecuteAsync(String query, Object parameters) => await ExecuteAsync(query, parameters);

        async Task<List<T>> IDbStoreService.SelectListAsync<T>(String query, Object parameters) =>
            await SelectListAsync<T, T>(query, parameters);

        async Task<List<TResult>> IDbStoreService.SelectListAsync<T, TResult>(String query, Object parameters) =>  
             await SelectListAsync<T, TResult>(query, parameters);
    }
}
