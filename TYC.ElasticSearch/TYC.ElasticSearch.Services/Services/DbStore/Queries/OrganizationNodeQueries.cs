﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Services.DbStore.Queries
{
    internal static class OrganizationNodeQueries
    {
        public const String GET_HIERARCHY =
@"SELECT * FROM dbo.GetExtendedAdminHierarchy(@userId)";

        public const String GET_USER =
@"   SELECT Id
          , Email
          , FirstName
          , LastName
          , IsAdmin
          , p.ParentNodeId AS FirstParent
       FROM dbo.Users u
OUTER APPLY (
	 SELECT TOP 1 ParentNodeId FROM dbo.OrganizationAllHierarchy oah
 INNER JOIN dbo.Companies c ON c.Id = oah.ParentNodeId
	  WHERE NodeId = u.Id
) p
      WHERE Id = @userId";

        public const String TASK_SEARCH_GET_HIERARCHY =
@"DECLARE @IsAdmin BIT = 0;
SELECT @IsAdmin = IsAdmin FroM dbo.Users u WHERE u.Id = @userId

;WITH AvailableIds AS (
	SELECT Id FROM dbo.GetExtendedAdminHierarchy(@userId)
), AllAvailableIds AS (
	 SELECT DISTINCT oh.Id 
	   FROM AvailableIds ids
 INNER JOIN dbo.OrganizationAllHierarchy oah ON ids.Id = oah.ParentNodeId
CROSS APPLY (VALUES(oah.NodeId), (oah.ParentNodeId)) oh(Id)
), AllAvailableIdsExceptPrivate AS (
	SELECT Id FROM AllAvailableIds
	EXCEPT 
	SELECT Id FROM dbo.Groups g WHERE @IsAdmin = 0 AND IsPublic = 0
)
SELECT * FROM AllAvailableIdsExceptPrivate
UNION 
SELECT * FROM AvailableIds";
    }
}
