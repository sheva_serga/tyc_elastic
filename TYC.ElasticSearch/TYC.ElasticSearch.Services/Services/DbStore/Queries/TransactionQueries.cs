﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Services.Queries
{
    internal static class TransactionQueries
    {
        public const String GET_TRANSACTIONS =
@"WITH NodeNames AS (
	SELECT c.Id AS NodeId, c.[Name] AS NodeName FROM dbo.Companies c 
	UNION ALL 
	SELECT c.Id,
	CASE
	    WHEN c.FirstName IS NOT NULL AND c.LastName IS NOT NULL  THEN CONCAT(c.FirstName, ' ', c.LastName)
	    WHEN c.OrganizationName IS NOT NULL  THEN c.OrganizationName
	    ELSE c.InvitedUserCompanyName  
	END AS [Name]
	FROM dbo.Users c
	UNION ALL 
	SELECT c.Id, c.[Name] FROM dbo.Groups c
)
SELECT 	  CONCAT('transaction_', t.Id) as Id
		, t.FinancingType
		, t.Archived
		, Property.AddressLine1			as 'Property.AddressLine1'
		, Property.AddressLine2			as 'Property.AddressLine2'
		, Property.Zip					as 'Property.Zip'				
		, Property.City					as 'Property.City'				
		, Property.CountyId				as 'Property.CountyId'			
		, Property.StateId				as 'Property.StateId'			
		, Property.CountyName			as 'Property.CountyName'		
		, Property.StateName			as 'Property.StateName'		
		, Property.StateAbbreviation	as 'Property.StateAbbreviation',
		    (SELECT  tp.Id
			 	   , tp.FileNumber
			 	   , nn.NodeName
			 	   , tp.OrganizationNodeId 
			    FROM dbo.TransactionParticipants tp
		   LEFT JOIN NodeNames nn ON nn.NodeId = tp.OrganizationNodeId
		       WHERE tp.Transaction_Id = t.Id FOR JSON PATH
			) AS 'Participants'
		
		, (SELECT c.Id, c.StartDate FROM dbo.Closings c WHERE c.Id = t.Id FOR JSON PATH) AS Closings
		, JSON_QUERY(PinNumbers.Id, '$.List') as 'PinNumbers'
		, JSON_QUERY(FolowerIds.Id, '$.List') as 'FolowerIds'
		 FROM dbo.Transactions t
OUTER APPLY (SELECT TOP 1 a.AddressLine1
		    , a.AddressLine2
			, a.Zip
			, a.City
			, a.CountyId
			, a.StateId
			, c.Name AS CountyName
			, s.Name AS StateName
			, s.Abbreviation AS StateAbbreviation
		 FROM dbo.Properties p 
   INNER JOIN dbo.Addresses a ON  a.Id = p.Address_Id
   INNER JOIN dbo.States s ON s.Id = a.StateId
    LEFT JOIN dbo.Counties c ON c.Id = a.CountyId
        WHERE p.Id = t.Property_Id) Property
OUTER APPLY (
	SELECT JSON_QUERY('[' + 
		STUFF(( SELECT ',' +  CONVERT(NVARCHAR(50), User_Id)  FROM dbo.FollowedTransactions WHERE Transaction_Id = t.Id FOR XML PATH('')),1,1,'') 
	+ ']') AS [List] FOR JSON PATH, WITHOUT_ARRAY_WRAPPER 
) FolowerIds(Id)
OUTER APPLY (
	SELECT JSON_QUERY('[' + 
		STUFF(( SELECT ',' + '""' + CONVERT(NVARCHAR(50), PIN) + '""' FROM dbo.PropertyPins WHERE PropertyId = t.Property_Id FOR XML PATH('')),1,1,'') 
	+ ']') AS[List] FOR JSON PATH, WITHOUT_ARRAY_WRAPPER 
) PinNumbers(Id)
FOR JSON PATH, INCLUDE_NULL_VALUES";
    }
}
