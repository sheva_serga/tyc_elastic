﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Services.Queries
{
    internal static class TasksQueries
    {
        public const String GET_TASKS =
@"SELECT CONCAT('task_', task.Id ) AS Id 
   , task.AssigneeId
   , task.ReceiverId
   , task.RequesterId
   , task.CreatorId
   , task.Archived 
   , task.IsDeleted 
   , CONCAT('transaction_', task.TransactionId) AS TransactionId
   , task.DueDate
   , task.DateType
   , task.Type
   , task.Status
  FROM dbo.Tasks task FOR JSON AUTO";

    }
}
