﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYC.JsonExport.Common.Models;
using TYC.JsonExport.Common.Models.SqlModels;
using TYC.JsonExport.Services.DbStore.Queries;
using TYC.JsonExport.Services.Interfaces;

namespace TYC.JsonExport.Services.DbStore
{
    internal class DbUserService : IDbUserService 
    {
        private readonly IDbStoreService _storeService;

        public DbUserService(IDbStoreService storeService)
        {
            _storeService = storeService;
        }

        public IUser GetUser(Int32 userId) =>
            _storeService.Select<UserDto>(
                  OrganizationNodeQueries.GET_USER
                , new { userId }
            );
    }
}
