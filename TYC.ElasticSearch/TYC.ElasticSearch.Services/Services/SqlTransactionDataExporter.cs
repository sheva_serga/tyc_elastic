﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYC.JsonExport.Common.Models.ElasticSearch;
using TYC.JsonExport.Services.DbStore;
using TYC.JsonExport.Services.Interfaces;
using TYC.JsonExport.Services.Queries;

namespace TYC.JsonExport.Services
{
    internal class SqlTransactionDataExporter : ITransactionExporter
    {
        private readonly IDbStoreService _storeService;

        public SqlTransactionDataExporter(IDbStoreService storeService)
        {
            _storeService = storeService;
        }

        public IEnumerable<Transaction> GetData()
        {
            StringBuilder builder = new StringBuilder();

            using (DbConnection connection = _storeService.CreateConnection())
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = TransactionQueries.GET_TRANSACTIONS;
                command.CommandTimeout = 0;
                connection.Open();

                using (DbDataReader reader = command.ExecuteReader())
                    while (reader.Read()) builder.Append(reader[0] as String);
        
                connection.Close();
            }

            return new JsonObjectEnumerator<Transaction>(builder.ToString()).ToList();
        }

        public async Task<IEnumerable<Transaction>> GetDataAsync()
        {
            StringBuilder builder = new StringBuilder();
            using (DbConnection connection = _storeService.CreateConnection())
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = TransactionQueries.GET_TRANSACTIONS;
                command.CommandTimeout = 0;

                connection.Open();

                using (DbDataReader reader = command.ExecuteReader())
                    while (await reader.ReadAsync()) builder.Append(reader[0] as String);

                connection.Close();
            }

            return new JsonObjectEnumerator<Transaction>(builder.ToString()).ToList();
        }
    }
}
