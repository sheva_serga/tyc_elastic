﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYC.JsonExport.Common.Models;
using TYC.JsonExport.Common.Models.ElasticSearch;

namespace TYC.JsonExport.Services.Search
{
    internal static class TaskSearchHelper
    {
        private static QueryContainer GetAssigneeCheckClaues(IUser user, QueryContainerDescriptor<TransactionTask> query) =>
            user.IsAdmin && user.FirstParent.HasValue
                ? query.Terms(t => t.Field(f => f.AssigneeId).Terms(new[] { user.Id, user.FirstParent.Value }))
                : query.Term(t => t.Field(f => f.AssigneeId).Value(user.Id));

        public static QueryContainer GetTaskGroupClauese(this QueryContainerDescriptor<TransactionTask> query, String groupNum, IUser user)
        {
            String dateTime = DateTime.Now.ToString("MM/dd/yyyy");
            String lastWeekDate = DateTime.Now.AddDays(-7).ToString("MM/dd/yyyy");

            switch (groupNum)
            {
                case "1":
                    return query.Term(t => t.Field(g => g.Status).Value(1));
                case "2":
                    return
                       query.Exists(t => t.Field(g => g.DueDate))
                    && query.DateRange(t => t
                           .Field(g => g.DueDate)
                           .LessThanOrEquals(dateTime)
                           .Format("MM/dd/yyyy")
                       )
                    && query.Terms(t => t
                           .Field(g => g.Status)
                           .Terms(new[] { 2, 4 })
                       );
                case "3":
                    return
                       query.Exists(t => t.Field(g => g.DueDate))
                    && query.DateRange(t => t
                           .Field(g => g.DueDate)
                           .GreaterThanOrEquals(dateTime)
                           .LessThanOrEquals(dateTime)
                           .Format("MM/dd/yyyy")
                       )
                    && query.Terms(t => t.Field(g => g.Status).Terms(new[] { 2, 4 }))
                    && GetAssigneeCheckClaues(user, query);
                case "4":
                    return
                       query.Exists(t => t.Field(g => g.DueDate))
                    && query.DateRange(t => t
                           .Field(g => g.DueDate)
                           .GreaterThanOrEquals(lastWeekDate)
                           .LessThanOrEquals(dateTime)
                           .Format("MM/dd/yyyy")
                       )
                    && query.Terms(t => t.Field(g => g.Status).Terms(new[] { 2, 4 }))
                    && GetAssigneeCheckClaues(user, query);
                case "5":
                    return
                       query.Exists(t => t.Field(g => g.DueDate))
                    && query.DateRange(t => t
                           .Field(g => g.DueDate)
                           .GreaterThan(lastWeekDate)
                           .Format("MM/dd/yyyy")
                       )
                    && query.Terms(t => t.Field(g => g.Status).Terms(new[] { 2, 4 }))
                    && GetAssigneeCheckClaues(user, query);
                case "6":
                    return
                       query.Bool(b => b.MustNot(m =>
                             m.Exists(t => t.Field(g => g.DueDate))
                          || m.Term(t => t.Field(f => f.DateType).Value(1))
                       ))
                    && query.Terms(t => t.Field(g => g.Status).Terms(new[] { 2, 4 }))
                    && GetAssigneeCheckClaues(user, query);
                case "7":
                    return
                       query.Bool(b => b
                           .MustNot(m => m
                               .Exists(t => t.Field(g => g.DueDate))
                           )
                       )
                    && query.Term(t => t.Field(f => f.DateType).Value(1))
                    && query.Terms(t => t.Field(g => g.Status).Terms(new[] { 2, 4 }))
                    && GetAssigneeCheckClaues(user, query);
                case "8":
                    return
                       query.Terms(t => t.Field(g => g.Status).Terms(new[] { 1, 2, 3, 4 }))
                    && query.Term(t => t.Field(g => g.CreatorId).Value(user.Id))
                    && query.Term(t => t.Field(g => g.Type).Value(2));
                case "9":
                    return
                       query.Term(t => t.Field(g => g.Status).Value(3));
                default:
                    return null;
            }
        }


        public static IAggregationContainer BuildAggregationContainer(this AggregationContainerDescriptor<TransactionTask> aggr, IUser user)
        {
            for (int i = 1; i <= 9; i++)
            {
                aggr = aggr.Filter(i.ToString(), f => f
                    .Filter(ff => ff.GetTaskGroupClauese(i.ToString(), user))
                    .Aggregations(a2 => a2.Terms("status", t => t.Field(ff => ff.Status)))
                );
            }

            return aggr;
        }
    }
}
