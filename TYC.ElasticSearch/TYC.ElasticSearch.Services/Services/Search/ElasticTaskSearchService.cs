﻿using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYC.JsonExport.Common;
using TYC.JsonExport.Common.Models;
using TYC.JsonExport.Common.Models.ElasticSearch;
using TYC.JsonExport.Services.Interfaces;

namespace TYC.JsonExport.Services.Search
{
    internal class ElasticTaskSearchService : ITaskSearchService
    {
        private readonly Uri _nodeUri;
        private readonly IDbOrganizationNodeService _nodeService;

        public ElasticTaskSearchService(String uri, IDbOrganizationNodeService nodeService)
        {
            _nodeUri = new Uri(uri);
            _nodeService = nodeService;
        }

        public ISearchResponse<TransactionTask> SearchTasks(IUser user, Int32 groupId)
        {
            var connectionSettings = new ConnectionSettings(_nodeUri)
                .DefaultIndex(Constants.INDEX_NAME);

            ElasticClient client = new ElasticClient(connectionSettings);

            Int32[] nodeIds = _nodeService.GetUserExtendedHierarchy(user.Id).ToArray();
            Int32[] nodeAllIds = _nodeService.GetTaskSearchHierarchy(user.Id).ToArray();

            ISearchResponse<TransactionTask> result = client.Search<TransactionTask>(s => s
                .Skip(0).Take(20)
                .Type<RelationDocumentBase>()
                .Index(Constants.INDEX_NAME)
                .Query(q1 => q1
                    .Bool(b => b
                        .Filter(f => f
                            .HasParent<Transaction>(t => t
                                .Query(chq => chq
                                    .Nested(n => n
                                        .Path(tp => tp.Participants)
                                        .Query(tp => tp
                                            .Bool(b2 => b2
                                                .Must(f2 => f2
                                                    .Terms(nt => nt
                                                        .Field(tp2 => tp2.Participants.First().OrganizationNodeId)
                                                        .Terms(nodeIds)
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            ) && (
                                   f.Terms(t2 => t2.Field(f1 => f1.AssigneeId).Terms(nodeAllIds))
                                || f.Terms(t2 => t2.Field(f1 => f1.RequesterId).Terms(nodeAllIds))
                                || f.Terms(t2 => t2.Field(f1 => f1.ReceiverId).Terms(nodeAllIds))
                                || f.Terms(t2 => t2.Field(f1 => f1.CreatorId).Terms(nodeAllIds))
                            ) && (
                                   f.Term(t2 => t2.Field(f1 => f1.IsDeleted).Value(false))
                                && f.Term(t2 => t2.Field(f1 => f1.Archived).Value(false))
                            )
                        )
                    )
                )
                .Aggregations(aggr => aggr.BuildAggregationContainer(user))
                .PostFilter(pf => pf.GetTaskGroupClauese(groupId.ToString(), user))
                .Sort(v => v
                    .Ascending(t => t.Status)
                    .Descending(t => t.Id)
                )
            );

            return result;
        }
    }
}
