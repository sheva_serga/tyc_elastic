﻿using System;
using System.Collections.Generic;
using Nest;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYC.JsonExport.Services.Interfaces;
using TYC.JsonExport.Common.Models.ElasticSearch;
using TYC.JsonExport.Common;
using TYC.JsonExport.Common.Models;

namespace TYC.JsonExport.Services.Search
{
    internal class ElasticTransactionSearchService : ITransactionSearchServise
    {
        private const String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

        private readonly Uri _nodeUri;
        private readonly IDbOrganizationNodeService _nodeService;

        public ElasticTransactionSearchService(String uri, IDbOrganizationNodeService nodeService)
        {
            _nodeUri = new Uri(uri);
            _nodeService = nodeService;
        }

        public ISearchResponse<Transaction> SearchTransactions(IUser user)
        {
            var connectionSettings = new ConnectionSettings(_nodeUri)
                .DefaultIndex(Constants.INDEX_NAME);

            ElasticClient client = new ElasticClient(connectionSettings);

            Int32[] nodeIds = _nodeService.GetUserExtendedHierarchy(user.Id).ToArray();

            ISearchResponse<Transaction> result = client.Search<Transaction>(s => s
                .Skip(0).Take(20)
                .Type<RelationDocumentBase>()
                .Index(Constants.INDEX_NAME)
                .Query(q => q
                    .Nested(n => n
                        .Path(p => p.Participants)
                        .Query(cd => cd
                            .Bool(con => con
                                .Filter(m => m
                                    .Terms(cond => cond
                                        .Field(f => f.Participants.First().OrganizationNodeId)
                                        .Terms(nodeIds)
                                    )
                                )
                            )
                        )
                    )
                )
                .Sort(v => v.Descending(t => t.Id))
            );

            return result;
        }
    }
}
