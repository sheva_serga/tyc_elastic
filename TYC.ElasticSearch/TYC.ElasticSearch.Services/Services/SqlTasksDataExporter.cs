﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYC.JsonExport.Common.Models.ElasticSearch;
using TYC.JsonExport.Services.DbStore;
using TYC.JsonExport.Services.Interfaces;
using TYC.JsonExport.Services.Queries;

namespace TYC.JsonExport.Services
{
    internal class SqlTasksDataExporter : ITasksExporter
    {
        private readonly IDbStoreService _storeService;

        public SqlTasksDataExporter(IDbStoreService storeService)
        {
            _storeService = storeService;
        }

        public IEnumerable<TransactionTask> GetData()
        {
            StringBuilder builder = new StringBuilder();
            using (DbConnection connection = _storeService.CreateConnection())
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = TasksQueries.GET_TASKS;
                command.CommandTimeout = 0;

                connection.Open();

                using (DbDataReader reader = command.ExecuteReader())
                    while (reader.Read()) builder.Append(reader[0] as String);

                connection.Close();
            }

            return new JsonObjectEnumerator<TransactionTask>(builder.ToString());
        }

        public async Task<IEnumerable<TransactionTask>> GetDataAsync()
        {
            StringBuilder builder = new StringBuilder();
            using (DbConnection connection = _storeService.CreateConnection())
            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = TasksQueries.GET_TASKS;
                command.CommandTimeout = 0;

                connection.Open();

                using (DbDataReader reader = command.ExecuteReader())
                    while (await reader.ReadAsync()) builder.Append(reader[0] as String);

                connection.Close();
            }

            return new JsonObjectEnumerator<TransactionTask>(builder.ToString());
        }
    }
}
