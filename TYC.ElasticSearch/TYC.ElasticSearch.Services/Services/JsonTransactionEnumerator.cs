﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TYC.JsonExport.Services
{
    internal class JsonObjectEnumerator<T> : IEnumerable<T> where T: class
    {
        private String _jsonString;

        public JsonObjectEnumerator(String jsonString)
        {
            _jsonString = jsonString;
        }

        public IEnumerator<T> GetEnumerator()
        {
            JsonSerializer serializer = new JsonSerializer();

            using (StringReader stringReader = new StringReader(_jsonString))
            using (JsonReader jsonReader = new JsonTextReader(stringReader))
                while (jsonReader.Read())
                    if (jsonReader.TokenType == JsonToken.StartObject)
                        yield return serializer.Deserialize<T>(jsonReader);
        }

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}
