﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Common.Models
{
    public interface IUser
    {
        Int32 Id { get; set; }
        String Email { get; set; }
        String FirstName { get; set; }
        String LastName { get; set; }
        Boolean IsAdmin { get; set; }
        Int32? FirstParent { get; set; }
    }
}
