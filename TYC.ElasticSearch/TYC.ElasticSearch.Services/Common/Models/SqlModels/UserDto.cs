﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TYC.JsonExport.Common.Models;

namespace TYC.JsonExport.Common.Models.SqlModels
{
    public class UserDto : IUser
    {
        public Int32 Id { get; set; }
        public String Email { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public Boolean IsAdmin { get; set; }
        public Int32? FirstParent { get; set; }
    }
}
