﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Common.Models.ElasticSearch
{
    [ElasticsearchType(Name = "transaction")]
    public class Transaction : RelationDocumentBase
    {
        [Number(Name = "finansing_type")]
        public Int32 FinansingType { get; set; }
        [Nested(Name = "property")]
        public PropertyInfo Property { get; set; }
        [Nested(Name = "closings")]
        public IEnumerable<Closing> Closings { get; set; }
        [Nested(Name = "participants")]
        public IEnumerable<TransactionParticipant> Participants { get; set; }

        [PropertyName("pin_numbers")]
        public IEnumerable<String> PinNumbers { get; set; }
        [PropertyName("folower_ids")]
        public IEnumerable<Int32> FolowerIds { get; set; }

        public Transaction()
        {
            Join = JoinField.Root<Transaction>();
        }
    }
}
