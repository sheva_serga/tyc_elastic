﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Common.Models.ElasticSearch
{
    public class TransactionParticipant
    {
        [Number(Name="id")]
        public Int32 Id { get; set; }
        [Text(Name = "node_name")]
        public String NodeName { get; set; }
        [Text(Name = "file_number")]
        public String FileNumber { get; set; }
        [Number(Name = "organization_id")]
        public Int32 OrganizationNodeId { get; set; }
    }
}
