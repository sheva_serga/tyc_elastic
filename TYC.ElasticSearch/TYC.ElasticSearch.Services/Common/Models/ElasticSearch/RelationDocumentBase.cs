﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Common.Models.ElasticSearch
{
    [ElasticsearchType(Name = "relational_data")]
    public abstract class RelationDocumentBase
    {
        [Text(Name = "id", Fielddata = true)]
        public String Id { get; set; }
        [Boolean(Name = "is_archived", Store = true)]
        public Boolean Archived { get; set; }
        [PropertyName("join_item")]
        public JoinField Join { get; set; }
    }
}
