﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Common.Models.ElasticSearch
{
    [ElasticsearchType(Name = "task")]
    public class TransactionTask : RelationDocumentBase
    {
        private String _transactionId;

        [Number(Name = "assignee_id")]
        public Int32 AssigneeId { get; set; }
        [Number(Name = "receiver_id")]
        public Int32 ReceiverId { get; set; }
        [Number(Name = "requester_id")]
        public Int32 RequesterId { get; set; }
        [Number(Name = "creator_id")]
        public Int32 CreatorId { get; set; }
        [Number(Name = "type")]
        public Int32 Type { get; set; }
        [Number(Name = "date_type")]
        public Int32 DateType { get; set; }
        [Number(Name = "status")]
        public Int32 Status { get; set; }
        [Boolean(Name = "is_deleted")]
        public Boolean IsDeleted { get; set; }
        [Date(Format = "date_hour_minute_second_millis||epoch_millis||date_hour_minute_second", Name = "due_date")]
        public DateTime? DueDate { get; set; }

        [Text(Name = "transaction_id")]
        public String TransactionId
        {
            get => _transactionId;
            set
            {
                _transactionId = value;
                Join = JoinField.Link<TransactionTask>(_transactionId);
            }
        }

    }
}
