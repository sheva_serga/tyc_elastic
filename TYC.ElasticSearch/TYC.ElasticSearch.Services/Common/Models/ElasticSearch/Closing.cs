﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Common.Models.ElasticSearch
{
    public class Closing
    {
        [Number(Name = "id")]
        public Int32 Id { get; set; }
        [Date(Format = "date_hour_minute_second_millis||epoch_millis||date_hour_minute_second", Name = "start_date")]
        public DateTime? StartDate { get; set; }
    }
}
