﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TYC.JsonExport.Common.Models.ElasticSearch
{
    public class PropertyInfo
    {
        [Text(Name = "address_line_1")]
        public String AddressLine1 { get; set; }
        [Text(Name = "address_line_2")]
        public String AddressLine2 { get; set; }
        [Text(Name = "zip")]
        public String Zip { get; set; }
        [Text(Name = "city")]
        public String City { get; set; }
        [Number(Name = "county_id")]
        public Int32? CountyId { get; set; }
        [Number(Name = "state_id")]
        public Int32? StateId { get; set; }
        [Text(Name = "county_name")]
        public String CountyName { get; set; }
        [Text(Name = "state_name")]
        public String StateName { get; set; }
        [Text(Name = "state_abbr")]
        public String StateAbbreviation { get; set; }
    }
}
